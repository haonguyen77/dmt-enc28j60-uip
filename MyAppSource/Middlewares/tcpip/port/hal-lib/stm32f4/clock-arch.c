/**
  ******************************************************************************
  * @file    clock-arch.c 
  * @author  Hao Nguen
  * @version
  * @date
  * @brief	
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "clock-arch.h"
#include "stm32f4xx_hal.h"

/* Private function prototypes -----------------------------------------------*/
clock_time_t
clock_time(void)
{
  return HAL_GetTick();
}
/*------------------------------- End ----------------------------------------*/