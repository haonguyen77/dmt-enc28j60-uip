/**
  ******************************************************************************
  * @file    clock-arch.h 
  * @author  Hao Nguyen
  * @version
  * @date
  * @brief
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __CLOCK_ARCH_H
#define __CLOCK_ARCH_H

/* Private define ------------------------------------------------------------*/
#define CLOCK_CONF_SECOND 1000

/* Private typedef -----------------------------------------------------------*/
typedef int clock_time_t;

#endif /* __CLOCK_ARCH_H */
/*------------------------------- End ----------------------------------------*/