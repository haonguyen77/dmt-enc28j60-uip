/**
  ******************************************************************************
  * @file    button_port.c 
  * @author  Hao Nguen
  * @version
  * @date
  * @brief	
  ******************************************************************************
  */
	
/* Includes ------------------------------------------------------------------*/
#include "AppPort/HAL_Lib_Cube/button_port.h"

/* Private variables ---------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/
inputState buttonReadInput(btnTypedef* btn){
	if(HAL_GPIO_ReadPin(btn->port, btn->pin) == GPIO_PIN_RESET){
		return ACTIVE;
	}else{
		return INACTIVE;
	}
}

void buttonLoop(void){
	static uint16_t count = 0;
	
	if(count < PERIODIC_SCAN){
		count ++;
	}else{
		count = 0;
	}
}
/*********************** End ***************************************************/