/**
  ******************************************************************************
  * @file    button_port.h 
  * @author  Hao Nguyen
  * @version
  * @date
  * @brief
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BUTTON_PORT_H
#define __BUTTON_PORT_H

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"
#include "AppSource/button.h"

/* Extern variables ----------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
#endif /* __BUTTON_PORT_H */
/*********************** End **************************************************/