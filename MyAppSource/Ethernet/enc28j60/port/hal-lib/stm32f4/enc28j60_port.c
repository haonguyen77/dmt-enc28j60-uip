/**
  ******************************************************************************
  * @file    enc28j60_port.c 
  * @author  Hao Nguen
  * @version
  * @date
  * @brief	
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "../MyAppSource/Ethernet/enc28j60/enc28j60.h"

/* Extern variables ----------------------------------------------------------*/
extern SPI_HandleTypeDef ENC_SPI;

/* Private variables ---------------------------------------------------------*/
static uint32_t iter_per_us = 0;
volatile uint32_t cnt;

/* Private function prototypes -----------------------------------------------*/
/**
   Calibrate the constant time
 **/
static void calibTime_us(void)
{
    uint32_t time;
    volatile uint32_t i;

    iter_per_us = 1000000;

    time = HAL_GetTick();
    /* Wait for next tick */
    while (HAL_GetTick() == time) {
        /* wait */
    }
    for (i=0; i<iter_per_us; i++) {
    }
		
		i = HAL_GetTick() - time;
		
		if(i > 0){
			iter_per_us = iter_per_us/(i*1000);
		}else{
			iter_per_us = 1;
		}
}

/**
 * @brief Init PHY Layer to communicate with ENC28J60
 * 
 */
void enc28j60PhyInit(void)
{
	calibTime_us();
	//Cube auto gennerates spi and gpio configuration.
}

/**
 * @brief Delay ms
 * 
 * @param time 
 */
void enc_delay_ms(uint32_t time)
{
	HAL_Delay(time);
}

/**
 * @brief Delay us
 * 
 * @param time 
 */
void enc_delay_us(uint16_t time){
	uint32_t delay = time*iter_per_us;
	for(cnt = 0; cnt < delay; cnt++);
}

/**
 * @brief disable Chip
 * 
 */
void disableChip(void)
{
	HAL_GPIO_WritePin(ENC_CS_PORT, ENC_CS_PIN, GPIO_PIN_SET);
}

/**
 * @brief enable Chip
 * 
 */
void enableChip(void)
{
	HAL_GPIO_WritePin(ENC_CS_PORT, ENC_CS_PIN, GPIO_PIN_RESET);
}

/**
 * @brief Send single byte to ENC28J60
 * 
 * @param dt 
 * @return unsigned char 
 */
unsigned char enc28j60SendByte(unsigned char dt){
	unsigned char recdt;
	HAL_SPI_TransmitReceive(&ENC_SPI, &dt, &recdt, 1, 1000);
  return recdt;
}
/*------------------------------- End ----------------------------------------*/