/**
  ******************************************************************************
  * @file    enc28j60_port.h 
  * @author  Hao Nguyen
  * @version
  * @date
  * @brief
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BUTTON_H
#define __BUTTON_H

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private define ------------------------------------------------------------*/
#define ENC_SPI				hspi1

#define ENC_CS_PIN		GPIO_PIN_4
#define ENC_CS_PORT		GPIOC

#define ENC_RST_PIN		GPIO_PIN_5
#define ENC_RST_PORT	GPIOC

#endif /* __ENC28J60_PORT_H */
/*------------------------------- End ----------------------------------------*/